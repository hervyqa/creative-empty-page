Extensions helper for create a blank page. Only support for inkscape 1.0.x

# Install

## GNU/Linux
- copy file `.inx` and `.py`.
- paste into `~/.config/inkscape/extensions/`

## Windows
- copy file `.inx`.
- paste into `C:\Program Files\Inkscape\share\inkscape\extensions\`

# How to Use

- File > New from new template
- choose "Branding and Social Media..." or "Creative Marketplace"

# Screenshoot

## Template Branding and Social Media

![branding-and-social-media](img/branding-social-media.png)

## Template Creative Marketplace

![branding-and-social-media](img/creative-marketplace.png)
